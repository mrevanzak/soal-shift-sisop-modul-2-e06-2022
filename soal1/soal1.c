#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#define SZ 100
#define SZARR 1000
#define SZBIG 10000

int jumlah_gacha=0, charIndex=0, weaponIndex=0;;
char characters[SZARR][SZ], weapons[SZARR][SZ];
time_t t;
char *characterDirPath = "/mnt/d/Fadel/Perkuliahan/Semester 4/SISOP/SHIFT_1/soal-shift-sisop-modul-2-e06-2022/soal1/characters";
char *weaponDirPath = "/mnt/d/Fadel/Perkuliahan/Semester 4/SISOP/SHIFT_1/soal-shift-sisop-modul-2-e06-2022/soal1/weapons";
char *startDir = "/mnt/d/Fadel/Perkuliahan/Semester 4/SISOP/SHIFT_1/soal-shift-sisop-modul-2-e06-2022/soal1";
char drive[2][SZ] = {"https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view", "https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view"};
char dir[3][SZ] = {"gacha_gacha", "characters", "weapons"};

void getFileFromServer(int i){
    char toDownload[SZ] = "https://docs.google.com/uc?export=download&id="; 
    char temp[SZ]; strcpy(temp, drive[i]);
    const char delim[2] = "/"; 
    char *token;
    char filename[SZ];

    token = strtok(temp, delim);
    while(token != NULL && !(strcmp(token, "d")==0)){
        token = strtok(NULL, delim);
    } token = strtok(NULL, "/");
    strcat(toDownload, token);
    
    sprintf(filename, "data%d.zip", i);
    char *argv[] = {"wget", "-q", "--no-check-certificate", toDownload, "-O", filename, NULL};

    time(&t);
    printf("%s-> Downloading %s\n", ctime(&t), drive[i]);
    execv("/usr/bin/wget", argv);
}

void unzipFile(int i){
    char filename[SZ];
    sprintf(filename, "data%d.zip", i);
    char *argv[] = {"unzip", "-q", "-o", filename, NULL};
    time(&t);
    printf("%s-> Unziping %s\n", ctime(&t), filename);
    execv("/usr/bin/unzip", argv);
}

void mkdirr(char dirName[]){
    char *argv[] = {"mkdir", "-p", dirName, NULL};
    time(&t);
    printf("%s-> Making directory %s\n", ctime(&t), dirName);
    execv("/usr/bin/mkdir", argv);
}

void downloadUnzipMakedir(){
    int status;
    pid_t child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // this is child
        getFileFromServer(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        pid_t child_id1 = fork();

        if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id1 == 0) {
            // this is child
            getFileFromServer(1);
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            
            pid_t child_id2 = fork();

            if (child_id2 < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id2 == 0) {
                // this is child
                unzipFile(0);
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                pid_t child_id3 = fork();

                if (child_id3 < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id3 == 0) {
                    // this is child
                    unzipFile(1);
                } else {
                    // this is parent
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 < 0) {
                        exit(EXIT_FAILURE);
                    }
                    if (child_id3 == 0) {
                        // this is child
                        mkdirr("gacha_gacha");
                    } else {
                        // this is parent
                        while ((wait(&status)) > 0);
                        return;
                    }
                }
            }
        }
    }
}

void formatedTime(int *s, int *m, int *h, char str[]){
    *s = *s + 1;
    if (*s > 59) {
        *m = *m + 1;
        *s = 0;
    }
    if (*m > 59) {
        *h = *h + 1;
        *m = 0;
    }
    if (*h > 23) {
        *s = *m = *h =  0;
    }
    if (*h < 10){
        sprintf(str, "0%d:", *h);
    }else {
        sprintf(str, "%d:", *h);
    }
    if (*m < 10){
        sprintf(str, "%s0%d:", str, *m);
    }else {
        sprintf(str, "%s%d:", str, *m);
    }
    if (*s < 10){
        sprintf(str, "%s0%d", str, *s);
    }else {
        sprintf(str, "%s%d", str, *s);
    }
}

char* jsonToString(char *dir, char *fileName){
    FILE *fp;
    char buffer[SZBIG];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;
    char path[SZARR];
    sprintf(path, "%s/%s", dir, fileName);
    fp = fopen(path,"r");
    fread(buffer, SZBIG, 1, fp);
    fclose(fp);
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    char *temp = malloc(sizeof(char)*SZARR);
    sprintf(temp, "_%s_%s_%s_", dir, json_object_get_string(rarity), json_object_get_string(name));
    return temp;
}

void jsonToVariabel(){
    DIR *dp;
    struct dirent *ep;

    dp = opendir(characterDirPath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                strcpy(characters[charIndex], jsonToString("characters", ep->d_name));
                charIndex++;
            }
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    dp = opendir(weaponDirPath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                strcpy(weapons[weaponIndex], jsonToString("weapons", ep->d_name));
                weaponIndex++;
            }
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void mkChangeDir(char *currDir){
    int status;
    pid_t child_temp = fork();
    if (child_temp < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_temp == 0){
        mkdirr(currDir);
    }else {
        while ((wait(&status)) > 0);
        chdir(currDir);
        return;
    }
}

void randomize(){
    chdir("gacha_gacha");
    int primogems = 79000;
    char currDir[SZBIG]="";
    char currTxt[SZBIG]="";
    FILE *fp;
    time(&t);
    struct tm *local = localtime(&t);
    int s=local->tm_sec-1, m=local->tm_min, h=local->tm_hour;
    while(primogems-160 > 0){
        primogems = primogems - 160;
        if (jumlah_gacha % 90 == 0){
            if ((strcmp(currDir, ""))){
                chdir("..");
            }

            /* round up */
            sprintf(currDir, "total_gacha_%d", jumlah_gacha+90); 

            /* sesuai jumlah data */
            // if (((primogems/160) > 89) )
            //     sprintf(currDir, "total_gacha_%d", jumlah_gacha+90);
            // else 
            //     sprintf(currDir, "total_gacha_%d", 1+jumlah_gacha+(((int)primogems)/160)); 
            mkChangeDir(currDir);
        }
        if (jumlah_gacha % 10 == 0){
            if (fp != NULL){
                fclose(fp);
            }
            char timeTemp[SZARR];
            formatedTime(&s, &m, &h, timeTemp);

            /* round up */
            sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, jumlah_gacha+10); 
            
            /* sesuai jumlah data */
            // if (((primogems/160) > 9) )
            //     sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, jumlah_gacha+10);
            // else 
            //     sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, 1+jumlah_gacha+(((int)primogems)/160));
            
            fp = fopen(currTxt, "a");
            if (fp == NULL){
                perror("error opening txt");
            }

        }
        if (((jumlah_gacha+1) & 1) && primogems > 0){
            long int randNum = abs(rand()*abs(rand()*rand())%charIndex)%charIndex;
            fprintf(fp, "%d%s%d\n", jumlah_gacha+1, characters[randNum], primogems);
        }else if (((jumlah_gacha+1) & 1) == 0 && primogems > 0){
            long int randNum = abs(rand()*abs(rand()*rand())%weaponIndex)%weaponIndex;
            fprintf(fp, "%d%s%d\n", jumlah_gacha+1, weapons[randNum], primogems);
        }
        jumlah_gacha++;
    }
    fclose(fp);
}

void dirToZip(char *filePath, char *dest){
    char *argv[] = {"zip", "-P", "satuduatiga", "-r", dest, filePath, "-q", NULL};
    time(&t);
    printf("%s-> Zipping file %s\n", ctime(&t), filePath);
    execv("/usr/bin/zip", argv);
}

void removeDir(char *fileName){
    char *argv[] = {"rm", "-rf", fileName, NULL};
    time(&t);
    printf("%s-> Removing file %s\n", ctime(&t), fileName);
    execv("/usr/bin/rm", argv);
}

void removerAllDir(){
    int status;
    pid_t child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        // this is child
        removeDir(dir[0]);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        pid_t child_id1 = fork();
        if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id1 == 0) {
            // this is child
            removeDir(dir[1]);
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            pid_t child_id2 = fork();
            if (child_id2 < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id2 == 0) {
                // this is child
                removeDir(dir[2]);
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                return;
            }
        }
    }
}

void task1(){
    int status;
    chdir(startDir);
    pid_t child_id1 = fork();
    if (child_id1 < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id1 == 0) {
        // this is child
        downloadUnzipMakedir();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        pid_t child_id2 = fork();
        if (child_id2 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id2 == 0) {
            // this is child
            jsonToVariabel();
            randomize();
        } else {
            // this is parent
            while ((wait(&status)) > 0);
        }
    }
    char *argv[] = {"ls", NULL};
    execv("/usr/bin/ls", argv);
}

void task2(){
    int status;
    chdir(startDir);
    pid_t child_id1 = fork();
    if (child_id1 < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id1 == 0) {
        // this is child
        dirToZip("gacha_gacha", "not_safe_for_wibu");
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        pid_t child_id2 = fork();
        if (child_id2 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id2 == 0) {
            // this is child
            removerAllDir();
        } else {
            // this is parent
            while ((wait(&status)) > 0); 
        }
    }
    char *argv[] = {"ls", NULL};
    execv("/usr/bin/ls", argv);
}

int main() {
    pid_t pid, sid;      
    pid = fork();  
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    while (1) {
        int status;
        time(&t);
        struct tm *local = localtime(&t);
        char timeFormated[SZ];
        strftime(timeFormated, sizeof(timeFormated), "%H:%M:%S_%d-%m", local);
        if (strcmp(timeFormated, "04:44:00_30-03")==0 || strcmp(timeFormated, "07:44:00_30-03")==0){
            pid_t child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0) {
                // this is child
                if (strcmp(timeFormated, "04:44:00_30-03")==0){
                    task1();
                }else if (strcmp(timeFormated, "07:44:00_30-03")==0) {
                    task2();
                }
            } else {
                // this is parent
                while ((wait(&status)) > 0); 
            }
        }
        sleep(1);
    }
}
