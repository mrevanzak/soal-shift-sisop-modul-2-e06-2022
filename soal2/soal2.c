#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <string.h>

void mkdir(char *locdir){
    char *argv[] = {"mkdir", "-p", locdir, NULL};
    execv("/usr/bin/mkdir", argv);
}

void unzip(char *zipdir, char *locdir){
    char *argv[] = {"unzip", "-d", locdir, zipdir, NULL};
    execv("/usr/bin/unzip", argv);
}

void filter(char *locdir){
    char *argv[] = {"find", locdir, "-mindepth", "1", "-type", "d", "-exec", "rm", "-rf", "{}", "+", NULL};
    execv("/usr/bin/find", argv);
}

void delSubStr(char *str, char *sub){
    char *p = str;
    while((p = strstr(p, sub))){
        memmove(p, p+strlen(sub), 1+strlen(p+strlen(sub)));
    }
}

void deletePhoto(char *locdir){
    char *argv[] = {"find", locdir, "-maxdepth", "1", "-type", "f", "-name", "*.png", "-exec", "rm", "{}", "+", NULL};
    execv("/usr/bin/find", argv);
}

void createFolder(char *locdir, char category[]){
    char folderName[100];
    strcpy(folderName, locdir);
    strcat(folderName, "/");
    strcat(folderName, category);

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0)
        mkdir(folderName);

    else{
        while((wait(&status)) > 0);
    	return;
    }
}

void copyPhoto(char *fileName, char *locdir, char name[], char release[], char category[]){
    char srcFileDir[100];
    strcpy(srcFileDir, locdir);
    strcat(srcFileDir, "/");
    strcat(srcFileDir, fileName);

    char destFileDir[100];
    strcpy(destFileDir, locdir);
    strcat(destFileDir, "/");
    strcat(destFileDir, category);
    strcat(destFileDir, "/");
    strcat(destFileDir, release);
    strcat(destFileDir, ";");
    strcat(destFileDir, name);
    strcat(destFileDir, ".png");

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0){
        char *argv[] = {"cp", srcFileDir, destFileDir, NULL};
        execv("/usr/bin/cp", argv);
    }

    else{
        while ((wait(&status)) > 0);
        return;
    }
}

void renamePhoto(char fileName[], char *locdir, char name[], char category[]){
    char newName[100];
    strcpy(newName, locdir);
    strcat(newName, "/");
    strcat(newName, category);
    strcat(newName, "/");
    strcat(newName, name);
    strcat(newName, ".png");

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0){
        char *argv[] = {"mv", fileName, newName, NULL};
        execv("/usr/bin/mv", argv);
    }

    else{
        while ((wait(&status)) > 0);
        return;
    }
}

void createData(char *locdir){
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (locdir)) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0){
                char subFolder[100];
                strcpy(subFolder, locdir);
                strcat(subFolder, "/");
                strcat(subFolder, ent->d_name);

                struct dirent **namelist;
                int n;

                n = scandir(subFolder, &namelist, NULL, alphasort);
                int i = 0;
                if (n == -1){
                    perror("scandir");
                }

                for (int i = 0; i < n; i++){
                    if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                        char name[100];
                        char release[100];

                        FILE *fp;
                        char dataFile[100];
                        strcpy(dataFile, locdir);
                        strcat(dataFile, "/");
                        strcat(dataFile, ent->d_name);
                        strcat(dataFile, "/");
                        strcat(dataFile, "data.txt");

                        char fileName[100];
                        strcpy(fileName, locdir);
                        strcat(fileName, "/");
                        strcat(fileName, ent->d_name);
                        strcat(fileName, "/");
                        strcat(fileName, namelist[i]->d_name);

                        delSubStr(namelist[i]->d_name, ".png");

                        char *split = strtok(namelist[i]->d_name, ";");
                        free(namelist[i]);
                        int i = 0;
                        while (split != NULL)
                        {
                            if (i == 0)
                                strcpy(release, split);
                            else if (i == 1)
                                strcpy(name, split);

                            split = strtok(NULL, ";");
                            i++;
                        }
                        renamePhoto(fileName, locdir, name, ent->d_name);

                        if(access(dataFile, F_OK) == -1){
                            fp = fopen(dataFile, "w");
                            fprintf(fp, "kategori : %s", ent->d_name);
                            fclose(fp);
                        }
                        fp = fopen(dataFile, "a");
                        fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", name, release);
                        fclose(fp);
                    }
                }
                free(namelist);
            }
        }
        closedir (dir);
    }
}

int main(){
    int status;
    char *locdir = "/home/mrevanzak/shift2/drakor";
    char *zipdir = "/home/mrevanzak/drakor.zip";

    pid_t child_id = fork();

    if (child_id < 0) exit(EXIT_FAILURE);

    if (child_id == 0) mkdir(locdir);

    else{
        while ((wait(&status)) > 0);
        pid_t child_id2 = fork();
        if (child_id2 < 0) exit(EXIT_FAILURE);
        
        if (child_id2 == 0) unzip(zipdir, locdir);
        
        else{
            while ((wait(&status)) > 0);
            pid_t child_id3 = fork();
            if (child_id3 < 0) exit(EXIT_FAILURE);
            
            if (child_id3 == 0) filter(locdir);
            
            else {
                while ((wait(&status)) > 0);
                DIR *dp;
                struct dirent *ep;

                dp = opendir(locdir);
                if (dp != NULL){
                    while ((ep = readdir(dp)) != NULL){
                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                            char filename[100];
                            strcpy(filename, ep->d_name);
                            delSubStr(filename, ".png");
                            
                            char name[100];
                            char release[100];
                            char category[100];

                            char *split = strtok(filename, "_;");
                            int i=0;
                            while(split != NULL){
                                if(i%3==0){
                                    strcpy(name, split);
                                }
                                else if(i%3==1){
                                    strcpy(release, split);
                                }
                                else if(i%3==2){
                                    strcpy(category, split);
                                    createFolder(locdir, category);
                                    copyPhoto(ep->d_name, locdir, name, release, category);
                                }
                                split = strtok(NULL, "_;");
                                i++;
                            }
                        }
                    }
                    closedir(dp);
                }
                while((wait(&status)) > 0);
                pid_t child_id4 = fork();
                if (child_id4 < 0) exit(EXIT_FAILURE);

                if (child_id4 == 0) deletePhoto(locdir);

                else{
                    while((wait(&status)) > 0);
                    createData(locdir);
                }
            }
        }
    }
}