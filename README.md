# SISOP Modul 2

## Daftar isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomer 1](#nomer-1)
  - [Soal 1.A](#1a)
  - [Soal 1.B](#1b)
  - [Soal 1.C](#1c)
  - [Soal 1.D](#1d)
  - [Soal 1.E](#1e)
  - [Hasil](#1hasil)
- [Nomer 2](#nomer-2)
  - [Soal 2.A](#2a)
  - [Soal 2.B](#2b)
  - [Soal 2.C](#2c)
  - [Soal 2.D](#2d)
  - [Soal 2.E](#2e)
  - [Hasil](#2hasil)
- [Nomer 3](#nomer-3)
  - [Soal 3.A](#3a)
  - [Soal 3.B](#3b)
  - [Soal 3.C](#3c)
  - [Soal 3.D](#3d)
  - [Soal 3.E](#3e)
  - [Hasil](#3hasil)
- [Kendala](#kendala)

## Anggota Kelompok

| NRP        | NAMA                       |
| ---------- | -------------------------- |
| 5025201260 | Fadel Pramaputra Maulana   |
| 5025201079 | Julio Geraldi Soeiono      |
| 5025201145 | Mochamad Revanza Kurniawan |

## Nomer 1

Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.

### 1.A

Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

Fungsi dibawah ini akan mendownload zip file menggunakan command bash `wget` dimana pada metode ini kami mengambil token pada link drive dan melakukan append pada link `https://docs.google.com/uc?export=download&id=`. Contoh pada `https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view` kami mengambil `1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp` sebagai token, sehingga link download pada metode ini ialah `https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp`.

```bash
void getFileFromServer(int i){
char toDownload[SZ] = "https://docs.google.com/uc?export=download&id=";
char temp[SZ]; strcpy(temp, drive[i]);
const char delim[2] = "/";
char \*token;
char filename[SZ];

    token = strtok(temp, delim);
    while(token != NULL && !(strcmp(token, "d")==0)){
        token = strtok(NULL, delim);
    } token = strtok(NULL, "/");
    strcat(toDownload, token);

    sprintf(filename, "data%d.zip", i);
    char *argv[] = {"wget", "-q", "--no-check-certificate", toDownload, "-O", filename, NULL};

    time(&t);
    printf("%s-> Downloading %s\n", ctime(&t), drive[i]);
    execv("/usr/bin/wget", argv);
}
```

Setelah melakukan download zip file, kita mengunzip file tersebut dengan fungsi dibawah ini.

```bash
void unzipFile(int i){
    char filename[SZ];
    sprintf(filename, "data%d.zip", i);
    char *argv[] = {"unzip", "-q", "-o", filename, NULL};
    time(&t);
    printf("%s-> Unziping %s\n", ctime(&t), filename);
    execv("/usr/bin/unzip", argv);
}
```

Kemudian membuat folder `gacha_gacha` dengan fungsi berikut ini.

```bash
void mkdirr(char dirName[]){
    char *argv[] = {"mkdir", "-p", dirName, NULL};
    time(&t);
    printf("%s-> Making directory %s\n", ctime(&t), dirName);
    execv("/usr/bin/mkdir", argv);
}
```

Kemudian menggabungkan fungsi-fungsi diatas menggunakan fork agar download dan unzip terurut. `while ((wait(&status)) > 0);` pada parent process digunakan untuk menunggu child process selesai.

```bash
void downloadUnzipMakedir(){
    int status;
    pid_t child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // this is child
        getFileFromServer(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        pid_t child_id1 = fork();

        if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id1 == 0) {
            // this is child
            getFileFromServer(1);
        } else {
            // this is parent
            while ((wait(&status)) > 0);

            pid_t child_id2 = fork();

            if (child_id2 < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id2 == 0) {
                // this is child
                unzipFile(0);
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                pid_t child_id3 = fork();

                if (child_id3 < 0) {
                    exit(EXIT_FAILURE);
                }
                if (child_id3 == 0) {
                    // this is child
                    unzipFile(1);
                } else {
                    // this is parent
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 < 0) {
                        exit(EXIT_FAILURE);
                    }
                    if (child_id3 == 0) {
                        // this is child
                        mkdirr("gacha_gacha");
                    } else {
                        // this is parent
                        while ((wait(&status)) > 0);
                        return;
                    }
                }
            }
        }
    }
}
```

Kemudian kami menyimpan semua data pada folder weapons dan characters pada variabel array bernama `weapons` dan `characters` dengan format parsing format `.json`. Untuk setiap data `.json` nya kami gunakan fungsi berikut ini dan melakukan formating nama dengan format `_[tipe-item]_{rarity}_{name}_` sesuai perintah 1.D nanti.

```bash
char* jsonToString(char *dir, char *fileName){
    FILE *fp;
    char buffer[SZBIG];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;
    char path[SZARR];
    sprintf(path, "%s/%s", dir, fileName);
    fp = fopen(path,"r");
    fread(buffer, SZBIG, 1, fp);
    fclose(fp);
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    char *temp = malloc(sizeof(char)*SZARR);
    sprintf(temp, "_%s_%s_%s_", dir, json_object_get_string(rarity), json_object_get_string(name));
    return temp;
}
```

Dengan menggunakan dirent semua file `.json` dapat diekstrak kevariabel `weapons` dan `characters` dengan fungsi berikut ini.

```bash
void jsonToVariabel(){
    DIR *dp;
    struct dirent *ep;

    dp = opendir(characterDirPath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                strcpy(characters[charIndex], jsonToString("characters", ep->d_name));
                charIndex++;
            }
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    dp = opendir(weaponDirPath);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                strcpy(weapons[weaponIndex], jsonToString("weapons", ep->d_name));
                weaponIndex++;
            }
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}
```

### 1.B

Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA.

Fungsi ini digunakan untuk melakukan gacha yang dimana pada statement `if` pertama digunakan untuk mengecek apakah jumlah gacha habis dibagi 90 atau tidak, jika iya maka akan membuat folder baru dan melakukan change directory ke folder tersebut. Pada statement `if` kedua digunakan untuk mengecek apakah jumlah gacha dapat habis dibagi 10 atau tidak, jika iya maka akan membuat file `.txt` baru dan mengeprint hasil gacha pada file tersebut. Pada statement `if` ketiga digunakan untuk mengecek apakah jumlah gacha genap atau ganjil, jika genap maka akan dilakukan gacha weapons, jika ganjil akan dilakukan gacha characters.

```bash
void randomize(){
    chdir("gacha_gacha");
    int primogems = 79000;
    char currDir[SZBIG]="";
    char currTxt[SZBIG]="";
    FILE *fp;
    time(&t);
    struct tm *local = localtime(&t);
    int s=local->tm_sec-1, m=local->tm_min, h=local->tm_hour;
    while(primogems-160 > 0){
        primogems = primogems - 160;
        if (jumlah_gacha % 90 == 0){
            if ((strcmp(currDir, ""))){
                chdir("..");
            }

            /* round up */
            sprintf(currDir, "total_gacha_%d", jumlah_gacha+90);

            /* sesuai jumlah data */
            // if (((primogems/160) > 89) )
            //     sprintf(currDir, "total_gacha_%d", jumlah_gacha+90);
            // else
            //     sprintf(currDir, "total_gacha_%d", 1+jumlah_gacha+(((int)primogems)/160));
            mkChangeDir(currDir);
        }
        if (jumlah_gacha % 10 == 0){
            if (fp != NULL){
                fclose(fp);
            }
            char timeTemp[SZARR];
            formatedTime(&s, &m, &h, timeTemp);

            /* round up */
            sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, jumlah_gacha+10);

            /* sesuai jumlah data */
            // if (((primogems/160) > 9) )
            //     sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, jumlah_gacha+10);
            // else
            //     sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, 1+jumlah_gacha+(((int)primogems)/160));

            fp = fopen(currTxt, "a");
            if (fp == NULL){
                perror("error opening txt");
            }

        }
        if (((jumlah_gacha+1) & 1) && primogems > 0){
            long int randNum = abs(rand()*abs(rand()*rand())%charIndex)%charIndex;
            fprintf(fp, "%d%s%d\n", jumlah_gacha+1, characters[randNum], primogems);
        }else if (((jumlah_gacha+1) & 1) == 0 && primogems > 0){
            long int randNum = abs(rand()*abs(rand()*rand())%weaponIndex)%weaponIndex;
            fprintf(fp, "%d%s%d\n", jumlah_gacha+1, weapons[randNum], primogems);
        }
        jumlah_gacha++;
    }
    fclose(fp);
}
```

### 1.C

Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12*gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha*{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

Untuk melakukan format penamaan pada folder terdapat pada fungsi `randomize()` sebagai berikut. Dengan variabel `jumlah_gacha` ditambah 90 yang artinya untuk menampung 90 gacha selanjutnya.

```bash
...
sprintf(currDir, "total_gacha_%d", jumlah_gacha+90);
...
```

Untuk melakukan format penamaan pada file `.txt` kami menggunakan fungsi sebagai berikut. Dimana pada fungsi ini satuan detik ditambah satu kemudian satuan menit dan jam dilakukan pengecekan jika melebihi batas menit dan jam sesuai format 24-jam.

```bash
void formatedTime(int *s, int *m, int *h, char str[]){
    *s = *s + 1;
    if (*s > 59) {
        *m = *m + 1;
        *s = 0;
    }
    if (*m > 59) {
        *h = *h + 1;
        *m = 0;
    }
    if (*h > 23) {
        *s = *m = *h =  0;
    }
    if (*h < 10){
        sprintf(str, "0%d:", *h);
    }else {
        sprintf(str, "%d:", *h);
    }
    if (*m < 10){
        sprintf(str, "%s0%d:", str, *m);
    }else {
        sprintf(str, "%s%d:", str, *m);
    }
    if (*s < 10){
        sprintf(str, "%s0%d", str, *s);
    }else {
        sprintf(str, "%s%d", str, *s);
    }
}
```

Pada fungsi `randomize()`.

```bash
....
formatedTime(&s, &m, &h, timeTemp);
sprintf(currTxt, "%s_gacha_%d.txt", timeTemp, jumlah_gacha+10);
...
```

### 1.D

Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

Pada fungsi `randomize()` dilakukan pengulangan hingga nilai primogems tidak bisa dikurangi dengan 160.

```bash
...
while(primogems-160 > 0){
    primogems = primogems - 160;
    ...
}
...
```

Untuk melakukan format penamaan setiap gacha kami menggunakan potongan kode pada fungsi `randomize()` berikut ini. Dimana pada fungsi ini di cek untuk jumlah gacha ganjil dan genap. `(jumlah_gacha+1)` dilakukan penambahan 1 pada variabel `jumlah_gacha` karena pada program ini kami menginisialisasi variabel tersebut dengan nol. Variabel array digunakan untuk menyimpan data pada masing masing folder, dan variabel `primogems` digunakan untuk menyimpan primogems yang tersisa.

```bash
if (((jumlah_gacha+1) & 1) && primogems > 0){
    long int randNum = abs(rand()*abs(rand()*rand())%charIndex)%charIndex;
    fprintf(fp, "%d%s%d\n", jumlah_gacha+1, characters[randNum], primogems);
}else if (((jumlah_gacha+1) & 1) == 0 && primogems > 0){
    long int randNum = abs(rand()*abs(rand()*rand())%weaponIndex)%weaponIndex;
    fprintf(fp, "%d%s%d\n", jumlah_gacha+1, weapons[randNum], primogems);
}
```

### 1.E

Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip).

Untuk melakukan zip dan menghapus folder kami menggunakan fungsi berikut ini.

```bash
void dirToZip(char *filePath, char *dest){
    char *argv[] = {"zip", "-P", "satuduatiga", "-r", dest, filePath, "-q", NULL};
    time(&t);
    printf("%s-> Zipping file %s\n", ctime(&t), filePath);
    execv("/usr/bin/zip", argv);
}
```

```bash
void removeDir(char *fileName){
    char *argv[] = {"rm", "-rf", fileName, NULL};
    time(&t);
    printf("%s-> Removing file %s\n", ctime(&t), fileName);
    execv("/usr/bin/rm", argv);
}
```

Maka untuk menghapus semua directory yang ada menggunakan fork dengan isinya merupakan fungsi `removeDir()`.

```bash
void removerAllDir(){
    int status;
    pid_t child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        // this is child
        removeDir(dir[0]);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        pid_t child_id1 = fork();
        if (child_id1 < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_id1 == 0) {
            // this is child
            removeDir(dir[1]);
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            pid_t child_id2 = fork();
            if (child_id2 < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id2 == 0) {
                // this is child
                removeDir(dir[2]);
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                return;
            }
        }
    }
}
```

Pada kasus ini kami membedakan `task1()` dan `task2()` agar kode enak dilihat dan mudah untuk dipanggil dimana `task1()` merupakan fungsi dari mendownload zip sampai melakukan gacha dan `task2()` merupakan fungsi untuk membuat zip dan menghapus semua folder. Digunakan daemon untuk melakukan eksekusi pada waktu tertentu.

```bash
int main() {
    pid_t pid, sid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        int status;
        time(&t);
        struct tm *local = localtime(&t);
        char timeFormated[SZ];
        strftime(timeFormated, sizeof(timeFormated), "%H:%M:%S_%d-%m", local);
        if (strcmp(timeFormated, "04:44:00_30-03")==0 || strcmp(timeFormated, "07:44:00_30-03")==0){
            pid_t child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_id == 0) {
                // this is child
                if (strcmp(timeFormated, "04:44:00_30-03")==0){
                    task1();
                }else if (strcmp(timeFormated, "07:44:00_30-03")==0) {
                    task2();
                }
            } else {
                // this is parent
                while ((wait(&status)) > 0);
            }
        }
        sleep(1);
    }
}
```

Dapat dilihat fork didalam `while()` digunakan agar eksekusi suatu task diselesaikan terlebih dahulu, karena untuk setiap task proses harus berhenti terlebih dahulu agar dapat mengeluarkan output.

### 1.Hasil
Berikut merupakan hasil jika task pada jam `04:44:00` tanggal `30_03` dieksekusi:

![image](/uploads/c470877560d5539f4d3d45bfe3868bb7/image.png)

Isi folder `characters`:

![image](/uploads/411e41af68a104f4e22ba9e9dd91c268/image.png)

Isi folder `weapons`:

![image](/uploads/0b4fa24af084de768a3779a8e5e20d7b/image.png)

Isi folder `gacha_gacha`:

![image](/uploads/6b7dbffba431b7dffd357eed17430946/image.png)

Isi salah satu folder pada folder `gacha_gacha`:

![image](/uploads/8371a15d12e34a8375ecd00ec933629c/image.png)

Isi salah satu dari file `.txt`:

![image](/uploads/c45fc501f41584897b36addfd72fb178/image.png)

Berikut merupakan hasil jika task pada jam `07:44:00` tanggal `30_03` dieksekusi:

![image](/uploads/da037b5a51e117162934fb619f128b97/image.png)

Isi dari file `not_safe_for_wibu.zip`:

![image](/uploads/2068e8f30e1183925d12004dcc5666c7/image.png)

Isi dari folder `gacha_gacha`:

![image](/uploads/7aeb9c7df13c3cceff15f72dd33c98d8/image.png)

Isi dari salah satu folder pada folder `gacha_gacha`:

![image](/uploads/d4f522e47c031b73366beed62af5eb8b/image.png)

Menggunakan password untuk mengakses salah satu file `.txt`:

![image](/uploads/12a90f698f9761e81d9ec4241bbe2ff7/image.png)


## Nomer 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

Berikut merupakan main function dari source code yang sudah dikerjakan. Di nomer ini perlu banyak mengeksekusi perintah execv yang mengakibatkan perlu beberapa kali fork
```c
int main(){
    int status;
    char *locdir = "/home/mrevanzak/shift2/drakor";
    char *zipdir = "/home/mrevanzak/drakor.zip";

    pid_t child_id = fork();

    if (child_id < 0) exit(EXIT_FAILURE);

    if (child_id == 0) mkdir(locdir);

    else{
        while ((wait(&status)) > 0);
        pid_t child_id2 = fork();
        if (child_id2 < 0) exit(EXIT_FAILURE);
        
        if (child_id2 == 0) unzip(zipdir, locdir);
        
        else{
            while ((wait(&status)) > 0);
            pid_t child_id3 = fork();
            if (child_id3 < 0) exit(EXIT_FAILURE);
            
            if (child_id3 == 0) filter(locdir);
            
            else {
                while ((wait(&status)) > 0);
                DIR *dp;
                struct dirent *ep;

                dp = opendir(locdir);
                if (dp != NULL){
                    while ((ep = readdir(dp)) != NULL){
                        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
                            char filename[100];
                            strcpy(filename, ep->d_name);
                            delSubStr(filename, ".png");
                            
                            char name[100];
                            char release[100];
                            char category[100];

                            char *split = strtok(filename, "_;");
                            int i=0;
                            while(split != NULL){
                                if(i%3==0){
                                    strcpy(name, split);
                                }
                                else if(i%3==1){
                                    strcpy(release, split);
                                }
                                else if(i%3==2){
                                    strcpy(category, split);
                                    createFolder(locdir, category);
                                    copyPhoto(ep->d_name, locdir, name, release, category);
                                }
                                split = strtok(NULL, "_;");
                                i++;
                            }
                        }
                    }
                    closedir(dp);
                }
                while((wait(&status)) > 0);
                pid_t child_id4 = fork();
                if (child_id4 < 0) exit(EXIT_FAILURE);

                if (child_id4 == 0) deletePhoto(locdir);

                else{
                    while((wait(&status)) > 0);
                    createData(locdir);
                }
            }
        }
    }
}
```

### 2.A
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

Untuk dapat mengextract zip ke folder tujuan, folder/direktori tujuan harus sudah ada. Oleh karena itu perlu dibuat terlebih dahulu menggunakan fungsi `mkdir` yang dibuat sendiri
```c
void mkdir(char *locdir){
    char *argv[] = {"mkdir", "-p", locdir, NULL};
    execv("/usr/bin/mkdir", argv);
}
```

Panggil fungsi `unzip`
```c
void unzip(char *zipdir, char *locdir){
    char *argv[] = {"unzip", "-d", locdir, zipdir, NULL};
    execv("/usr/bin/unzip", argv);
}
```

Lalu karena terdapat folder-folder yang tidak penting. Dibuatlah fungsi `filter` untuk menghapus folder-folder tidak penting tersebut. Fungsi ini akan menghapus apapun yang bertipe folder di dalam direktori /home/[user]/shift2/drakor
```c
void filter(char *locdir){
    char *argv[] = {"find", locdir, "-mindepth", "1", "-type", "d", "-exec", "rm", "-rf", "{}", "+", NULL};
    execv("/usr/bin/find", argv);
}
```

### 2.B
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam `/drakor/romance`, jenis drama korea action akan disimpan dalam `/drakor/action` , dan seterusnya.

```c
DIR *dp;
struct dirent *ep;

dp = opendir(locdir);
if (dp != NULL){
    while ((ep = readdir(dp)) != NULL){
        if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
            char filename[100];
            strcpy(filename, ep->d_name);
            delSubStr(filename, ".png");
                            
            char name[100];
            char release[100];
            char category[100];

            char *split = strtok(filename, "_;");
            int i=0;
            while(split != NULL){
                if(i%3==0){
                    strcpy(name, split);
                }
                else if(i%3==1){
                    strcpy(release, split);
                }
                else if(i%3==2){
                    strcpy(category, split);
                    createFolder(locdir, category);
                    copyPhoto(ep->d_name, locdir, name, release, category);
                }
                split = strtok(NULL, "_;");
                i++;
            }
        }
    }
    closedir(dp);
}
```
Pertama membuka folder drakor menggunakan fungsi bawaan C yaitu `opendir` dan baca filenya menggunakan `readdir`. Baca setiap nama file yang ada satu per satu. copy nama file ke string `filename` kemudian hapus ".png" dari string menggunakan fungsi `delSubString`. 
```c
void delSubStr(char *str, char *sub){
    char *p = str;
    while((p = strstr(p, sub))){
        memmove(p, p+strlen(sub), 1+strlen(p+strlen(sub)));
    }
}
```

Nama file diformat dengan nama;tahun;kategori lalu terdapat juga 2 judul dalam 1 file yang dipisahkan dengan underscore (_). Kita gunakan fungsi `strtok` untuk memisahkan string2 tersebut. Diurutan pertama adalah nama jadi copy ke string `nama`, kedua adalah tahun jadi copy ke string `release`, yang ketiga adalah kategory jadi copy ke string `category`. Panggil fungsi `createFolder` untuk membuat folder berdasarkan isi dari string `category` tadi.
```c
void createFolder(char *locdir, char category[]){
    char folderName[100];
    strcpy(folderName, locdir);
    strcat(folderName, "/");
    strcat(folderName, category);

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0)
        mkdir(folderName);

    else{
        while((wait(&status)) > 0);
    	return;
    }
}
```

### 2.C
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: `/drakor/romance/start-up.png`.

Melanjutkan no 2.B tadi, setelah dibuat folder menggunakan fungsi `createFolder`. File langsung dicopy ke folder category yang sesuai dengan fungsi `copyPhoto`
```c
void copyPhoto(char *fileName, char *locdir, char name[], char release[], char category[]){
    char srcFileDir[100];
    strcpy(srcFileDir, locdir);
    strcat(srcFileDir, "/");
    strcat(srcFileDir, fileName);

    char destFileDir[100];
    strcpy(destFileDir, locdir);
    strcat(destFileDir, "/");
    strcat(destFileDir, category);
    strcat(destFileDir, "/");
    strcat(destFileDir, release);
    strcat(destFileDir, ";");
    strcat(destFileDir, name);
    strcat(destFileDir, ".png");

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0){
        char *argv[] = {"cp", srcFileDir, destFileDir, NULL};
        execv("/usr/bin/cp", argv);
    }

    else{
        while ((wait(&status)) > 0);
        return;
    }
}
```
Sampai disini, format nama belum sesuai dengan yang diminta soal karena nama file diatur dengan format tahun;nama.png. Dibuat seperti ini agar file tersortir secara otomatis untuk mengerjakan no 2.E

### 2.D
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

Karena tadi yang digunakan adalah copy bukan move jadi problem untuk nomer ini sudah teratasi. Namun foto-foto tersebut akan masih tetap ada di direktori `/drakor/` jadi perlu dihapus menggunakan fungsi deletePhoto
```c
void deletePhoto(char *locdir){
    char *argv[] = {"find", locdir, "-maxdepth", "1", "-type", "f", "-name", "*.png", "-exec", "rm", "{}", "+", NULL};
    execv("/usr/bin/find", argv);
}
```

Fungsi ini akan menghapus semua file berformat .png dengan maksimal kedalaman 1 jadi foto-foto yang ada di dalam sub folder kategori tidak akan ikut terhapus

### 2.E
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.
		
kategori : romance

nama : start-up<br>
rilis  : tahun 2020

nama : twenty-one-twenty-five<br>
rilis  : tahun 2022

```c
void createData(char *locdir){
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (locdir)) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0){
                char subFolder[100];
                strcpy(subFolder, locdir);
                strcat(subFolder, "/");
                strcat(subFolder, ent->d_name);

                struct dirent **namelist;
                int n;

                n = scandir(subFolder, &namelist, NULL, alphasort);
                int i = 0;
                if (n == -1){
                    perror("scandir");
                }

                for (int i = 0; i < n; i++){
                    if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                        char name[100];
                        char release[100];

                        FILE *fp;
                        char dataFile[100];
                        strcpy(dataFile, locdir);
                        strcat(dataFile, "/");
                        strcat(dataFile, ent->d_name);
                        strcat(dataFile, "/");
                        strcat(dataFile, "data.txt");

                        char fileName[100];
                        strcpy(fileName, locdir);
                        strcat(fileName, "/");
                        strcat(fileName, ent->d_name);
                        strcat(fileName, "/");
                        strcat(fileName, namelist[i]->d_name);

                        delSubStr(namelist[i]->d_name, ".png");

                        char *split = strtok(namelist[i]->d_name, ";");
                        free(namelist[i]);
                        int i = 0;
                        while (split != NULL)
                        {
                            if (i == 0)
                                strcpy(release, split);
                            else if (i == 1)
                                strcpy(name, split);

                            split = strtok(NULL, ";");
                            i++;
                        }
                        renamePhoto(fileName, locdir, name, ent->d_name);

                        if(access(dataFile, F_OK) == -1){
                            fp = fopen(dataFile, "w");
                            fprintf(fp, "kategori : %s", ent->d_name);
                            fclose(fp);
                        }
                        fp = fopen(dataFile, "a");
                        fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", name, release);
                        fclose(fp);
                    }
                }
                free(namelist);
            }
        }
        closedir (dir);
    }
}
```
Pertama `opendir` direktori `/drakor/` lalu `readdir` untuk membaca setiap folder kategori. Isi dari folder kategori dibaca menggunakan fungsi `scandir`. `readdir` secara default akan membaca satu per satu secara random sedangkan yang dibutuhkan adalah membaca secara terutut jadi digunakan fungsi `scandir`. Susun path untuk menyimpan data.txt dan untuk nama file yang kemudian disimpan ke string `dataFile` dan `fileName`. Dalam string fileName masih terdapat .png jadi perlu dihapus dengan fungsi `delSubString` seperti sebelumnya. Lalu format nama yang sebelumnya diatur menjadi tahun;nama perlu kita pisahkan dengan `strtok` dengan semicolon (;) sebagai delimiternya. Tampung yang pertama ke string `release` dan yang kedua ke string `name`. Jangan lupa rename foto agar sesuai dengan format nama di nomer 2.C menggunakan fungsi `renamePhoto` 
```c
void renamePhoto(char fileName[], char *locdir, char name[], char category[]){
    char newName[100];
    strcpy(newName, locdir);
    strcat(newName, "/");
    strcat(newName, category);
    strcat(newName, "/");
    strcat(newName, name);
    strcat(newName, ".png");

    int status;
    pid_t child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);

    if (child_id == 0){
        char *argv[] = {"mv", fileName, newName, NULL};
        execv("/usr/bin/mv", argv);
    }

    else{
        while ((wait(&status)) > 0);
        return;
    }
}
```

Saat file data.txt belum ada, akan dijalankan fungsi `fopen` dengan tipe write kemudian isinya diprint dengan nama kategori
```c
fp = fopen(dataFile, "w");
fprintf(fp, "kategori : %s", ent->d_name);
fclose(fp);
```

Saat file data.txt sudah ada, akan dijalankan fungsi `fopen` dengan tipe append kemudian isinya diprint dengan nama dan tahun rilis sesuai format yang diminta soal
```c
fp = fopen(dataFile, "a");
fprintf(fp, "\n\nnama : %s\nrilis : tahun %s", name, release);
fclose(fp);
```

### 2.Hasil
![image](/uploads/a628fcee7b1888fc608c0aa41d6f748c/image.png)
![image](/uploads/15c76390ef1e53c818df29df86248804/image.png)

## Nomer 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

Berikut merupakan main function dari source code yang sudah dikerjakan.

```c
int main(){
	char *locdir = "/home/mrevanzak/modul2";
	char *zipdir = "/home/mrevanzak/animal.zip";

	createFolder(locdir, "darat");
	sleep(3);
	createFolder(locdir, "air");
	unzip(zipdir, locdir);

	DIR *dp;
	struct dirent *ep;
	dp = opendir(locdir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "darat") != 0 && strcmp(ep->d_name, "air") != 0){
				if (strstr(ep->d_name, "darat") != NULL){
					movePhoto(ep->d_name, "darat", locdir);
				}
				else if (strstr(ep->d_name, "air") != NULL){
					movePhoto(ep->d_name, "air", locdir);
				}
				else{
					deletePhoto(ep->d_name, locdir);
				}
			}
		}
	}
	closedir(dp);

	char daratDir[100];
	strcpy(daratDir, locdir);
	strcat(daratDir, "/darat");
	dp = opendir(daratDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				if (strstr(ep->d_name, "bird") != NULL){
					deletePhoto(ep->d_name, daratDir);
				}
			}
		}
	}
	closedir(dp);

	char airDir[100];
	strcpy(airDir, locdir);
	strcat(airDir, "/air");
	dp = opendir(airDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			struct stat info;
			int r;
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				char listFile[100];
				strcpy(listFile, airDir);
				strcat(listFile, "/list.txt");

				char pathFile[100];
				strcpy(pathFile, airDir);
				strcat(pathFile, "/");
				strcat(pathFile, ep->d_name);
				
				FILE *fp = fopen(listFile, "a");
				r = stat(pathFile, &info);

				fprintf(fp, "%s_", getpwuid(info.st_uid)->pw_name);

				if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rwx");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR)
					fprintf(fp, "rw");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rx");
				else if (info.st_mode & S_IRUSR)
					fprintf(fp, "r");
				else if (info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "wx");
				else if (info.st_mode & S_IWUSR)
					fprintf(fp, "w");
				else if (info.st_mode & S_IXUSR)
					fprintf(fp, "x");

				char *name = strtok(ep->d_name, "_");
				fprintf(fp, "_%s\n", name);
				fclose(fp);
			}
		}
	}
	closedir(dp);
}
```

### 3.A
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

```c
void createFolder(char *locdir, char *category){
	char folderName[100];
	strcpy(folderName, locdir);
	strcat(folderName, "/");
	strcat(folderName, category);

	printf("%s\n", folderName);

	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

	if (child_id == 0)
	{
		char *argv[] = {"mkdir", "-p", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	else
	{
		while ((wait(&status)) > 0)
			;
		return;
	}
}
```
Untuk no 3a kita harus membuat fungsi yang akan membantu kita dalam membuat folder air dan darat. Dari sini kita akan mengambil lokasi direktori default yang kemudian akan kita copy ke array bernama `folderName`, kita juga akan mengambil nama category untuk "air" dan "darat" lalu akan kita fork dimana jika child hasil fork < 0 maka kita exit dan jika child id == 0 maka akan kita make directory dan mengambil `folderName` tadi untuk kategori folder "darat" dan "air". Lalu kita spesifikasi lokasi mkdir menggunakan execv yaitu di /bin/mkdir. Kalau sudah nanti di main kita tambahkan sleep(3) untuk 3 detik.

### 3.B
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”

```c
void unzip(char *zipdir, char *locdir){
	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"unzip", "-j", zipdir, "animal/*", "-d", locdir, NULL};
		execv("/bin/unzip", argv);
	}
	else{
		while ((wait(&status)) > 0);
		return;
	}
}
```
Untuk no 3b kita gunakan unzip untuk mengekstrak file zip soal. Disini kita akan mengambil zip directory file zip yang telah kita download sebagai zipdir dan location dir untuk menaruh hasil ekstraknya. Sama seperti nomor sebelumnya kita fork lalu unzip dan -d untuk spesifikasi direktori hasilnya nanti lalu kita execv untuk mengambil lokasi unzip di linux.

### 3.C
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder `/home/[USER]/modul2/darat` dan untuk hewan air dimasukkan ke folder `/home/[USER]/modul2/air`. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

```c
DIR *dp;
struct dirent *ep;
dp = opendir(locdir);
if (dp != NULL){
	while ((ep = readdir(dp)) != NULL){
		if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "darat") != 0 && strcmp(ep->d_name, "air") != 0){
			if (strstr(ep->d_name, "darat") != NULL){
				movePhoto(ep->d_name, "darat", locdir);
			}
			else if (strstr(ep->d_name, "air") != NULL){
				movePhoto(ep->d_name, "air", locdir);
			}
			else{
				deletePhoto(ep->d_name, locdir);
			}
		}
	}
}
closedir(dp);
```
Untuk no 3c kita diminta untuk memisahkan hewan darat dan air sesuai dengan folder "air" dan "darat" yang telah kita buat sebelumnya. Disini kita meminta bantuan DIR untuk directorynya untuk mengakses directorynya dan kemudian kita akan baca isi directorynya menggunakan readdir dan kemudian akan kita compare yang mana jika nama isi hasil ekstraknya ada string "darat" maka akan dipindah ke folder darat, jika air maka ke folder air, dan selain itu akan dihapus dengan fungsi `deletePhoto`. 
```c
void deletePhoto(char *fileName, char *locdir){
	char fileDir[100];
	strcpy(fileDir, locdir);
	strcat(fileDir, "/");
	strcat(fileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"rm", fileDir, NULL};
		execv("/bin/rm", argv);
	}
	else{
		while ((wait(&status)) > 0)
			;
		return;
	}
}
```

Untuk memindahkannya menggunakan fungsi `movePhoto`
```c
void movePhoto(char *fileName, char *category, char *locdir){
	char srcFileDir[100];
	strcpy(srcFileDir, locdir);
	strcat(srcFileDir, "/");
	strcat(srcFileDir, fileName);

	char destFileDir[100];
	strcpy(destFileDir, locdir);
	strcat(destFileDir, "/");
	strcat(destFileDir, category);
	strcat(destFileDir, "/");
	strcat(destFileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"mv", srcFileDir, destFileDir, NULL};
		execv("/bin/mv", argv);
	}
	else{
		while ((wait(&status)) > 0);
		return;
	}
}
```

Untuk movePhoto kita akan meminta bantuan command mv di linux. Kita akan membuat 2 array yang pertama untuk source directory dan kedua untuk destination directory yang dimana source akan membawa lokasi directory file hewan darat/air dan kemudian akan ditaruh sesuai foldernya masing".

### 3.D
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

```c
void deletePhoto(char *fileName, char *locdir){
	char fileDir[100];
	strcpy(fileDir, locdir);
	strcat(fileDir, "/");
	strcat(fileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"rm", fileDir, NULL};
		execv("/bin/rm", argv);
	}
	else{
		while ((wait(&status)) > 0)
			;
		return;
	}
}


char daratDir[100];
strcpy(daratDir, locdir);
strcat(daratDir, "/darat");
dp = opendir(daratDir);
if (dp != NULL){
	while ((ep = readdir(dp)) != NULL){
		if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
			if (strstr(ep->d_name, "bird") != NULL){
				deletePhoto(ep->d_name, daratDir);
			}
		}
	}
}
closedir(dp);
```
Untuk no 3d kita harus menghapus file nama yang terdapat birdnya, dari sini kita membuat array direktori darat karena tentunya file bird terdapat pada darat maka kita harus mengakses file darat. Lalu kita akan mengakses direktori darat dan kita read yang kemudian kita cek apakah ada string "bird" pada filename hasil ekstrak dan jika ada maka kita delete. Pada `deletePhoto` kita meminta bantuan rm untuk menghapus file yang ada "bird".

### 3.E
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

```c
char airDir[100];
strcpy(airDir, locdir);
strcat(airDir, "/air");
dp = opendir(airDir);
if (dp != NULL){
	while ((ep = readdir(dp)) != NULL){
		struct stat info;
		int r;
		if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
			char listFile[100];
			strcpy(listFile, airDir);
			strcat(listFile, "/list.txt");

			char pathFile[100];
			strcpy(pathFile, airDir);
			strcat(pathFile, "/");
			strcat(pathFile, ep->d_name);
			
			FILE *fp = fopen(listFile, "a");
			r = stat(pathFile, &info);

			fprintf(fp, "%s_", getpwuid(info.st_uid)->pw_name);

			if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
				fprintf(fp, "rwx");
			else if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR)
				fprintf(fp, "rw");
			else if (info.st_mode & S_IRUSR && info.st_mode & S_IXUSR)
				fprintf(fp, "rx");
			else if (info.st_mode & S_IRUSR)
				fprintf(fp, "r");
			else if (info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
				fprintf(fp, "wx");
			else if (info.st_mode & S_IWUSR)
				fprintf(fp, "w");
			else if (info.st_mode & S_IXUSR)
				fprintf(fp, "x");

			char *name = strtok(ep->d_name, "_");
			fprintf(fp, "_%s\n", name);
			fclose(fp);
		}
	}
}
closedir(dp);
```

Pertama akses direktori air dengan `opendir` lalu baca isinya menggunakan `readdir`. Lalu inisialisasi path dari file txt dan buat filenya menggunakan fopen dengan tipe append. Untuk mengakses UID bisa menggunakan command `getpwuid(info.st_uid)->pw_name)`, kemudian tinggal print ke dalam file txt dengan `fprintf`. Untuk file permission kita buat if else condition boolean yang akan print sesuai dengan kondisi yang true. Yang terakhir, untuk nama bisa split dari nama file dan print bagian namanya.

### 3.Hasil
![image](/uploads/0d40771dd08b863e323e9d7e326dc3f2/image.png)
![image](/uploads/0605044e855ae61216602ff9a6dc543b/image.png)


## Kendala
1. Bingung menggunakan daemon karena kadang tidak mengeluarkan output tetapi sudah terjawab dengan menggunakan fork.
